use std::net::UdpSocket;

fn main() {
    let socket = UdpSocket::bind("127.0.0.1:8080").expect("couldn't bind to address");
    socket.connect("127.0.0.1:5432").expect("connect function failed");
    let num1 = 2000i32.to_be_bytes();
    let num2 = (-20000 as i32).to_be_bytes();
    socket.send(&num1);
    socket.send(&num2);
    socket.send("add".as_bytes());
    println!("msg sent");
    let mut answer_bytes = [0; 4];
    socket.recv(&mut answer_bytes).expect("Didn't receive data");
    println!("got msg back");

    let mut answer_is_negative = false;
    let mut answer = 0;

    if answer_bytes[0] >= 128  {
        answer_bytes[0] -= 128;
        answer = std::i32::MAX;
        answer_is_negative = true; 
    }

    for i in 0..4 {
        if answer_is_negative { answer -= 256_i32.pow(3-i)*(answer_bytes[i as usize] as i32); }
        else { answer += 256_i32.pow(3-i)*(answer_bytes[i as usize] as i32); }
    }

    if answer_is_negative {
        answer = -answer-1;
    }

    println!("Answer: {}", answer);
}