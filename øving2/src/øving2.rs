use std::thread;
use std::time;
mod workers;

fn main() {
    let mut workers = workers::Workers::new(4);
    let mut event_loop = workers::Workers::new(1);

    // Event loop
    event_loop.post(fake_task);
    event_loop.post(fake_task);
    event_loop.post_timeout(fake_task, 1000);
    event_loop.post(fake_task);
    event_loop.post(fake_task);

    // Workers
    workers.post(fake_task);
    workers.post(fake_task);
    workers.post_timeout(fake_task, 2000);
    workers.post(fake_task);
    workers.post(fake_task);

    // End
    workers.end();
    event_loop.end();
}

fn fake_task() {
    let three_seconds = time::Duration::from_millis(3000);
    thread::sleep(three_seconds);
}
