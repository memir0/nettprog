// In Rust we need to tell it where things are from,
// in this case we are using the read_to_string method
// so we need to bring in the std::io::Read
// module to the party. We also need TcpListener and
// TcpStream
use std::fs::File;
use std::io::{BufRead, BufReader, Read, Write};
use std::net::{TcpListener, TcpStream};
use std::thread;

// Everything is the same in here
fn main() {
    let mut threads = Vec::new();
    let listener = TcpListener::bind("127.0.0.1:5432").unwrap();
    // The .0 at the end is indexing a tuple, FYI
    loop {
        let stream = listener.accept().unwrap().0;
        threads.push(thread::spawn(move || {
            handle_request(stream);
        }))
    }
}

// Things change a bit in here
fn handle_request(stream: TcpStream) {
    let mut reader = BufReader::new(stream);

    let mut http_request_definition = String::new();
    let _result = reader.by_ref().read_line(&mut http_request_definition);
    let http_request_definition_split: Vec<&str> =
        http_request_definition.split_whitespace().collect();
    println!("{}", http_request_definition);

    for line in reader.by_ref().lines() {
        let line_uw = line.unwrap();
        println!("{}", line_uw);
        if line_uw == "" {
            break;
        }
    }
    if http_request_definition_split[1] == "/favicon.ico" {
        let mut file_bytes_vec: Vec<u8> = Vec::new();

        let response = "HTTP/1.1 200 OK\n\n";
        for byte in response.as_bytes() {
            file_bytes_vec.push(*byte);
        }

        let mut file = File::open("rust-logo.png").unwrap();
        let _result = file.read_to_end(&mut file_bytes_vec);
        //let temp = &file_bytes_vec; // b: &Vec<u8>
        let file_bytes: &[u8] = &file_bytes_vec;
        reader.into_inner().write_all(file_bytes).unwrap();
    } else {
        send_response(reader.into_inner());
    }
}

// New function to write back with!
fn send_response(mut stream: TcpStream) {
    // Write the header and the html body
    let response = "HTTP/1.1 200 OK\n\n<html><body>Hello, World!</body></html>";
    stream.write_all(response.as_bytes()).unwrap();
}
