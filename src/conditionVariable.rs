use std::sync::{Arc, Mutex};
use std::thread;
use std::time;

fn main() {
    //let mut wait = true;
    let wait = Arc::new(Mutex::new(true));

    let wait_clone = wait.clone();
    let t = thread::spawn(move || {
        let twenty_milis = time::Duration::from_millis(20);
        println!("Thread started");
        while *wait_clone.lock().unwrap() {
            thread::sleep(twenty_milis);
        }
        println!("Done waiting");
    });

    let one_second = time::Duration::from_millis(1000);
    thread::sleep(one_second);

    *wait.lock().unwrap() = false;

    let _result = t.join();
}
