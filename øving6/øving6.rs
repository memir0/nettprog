extern crate mysql;
use chrono::naive::{NaiveDate, NaiveDateTime};
use mysql::prelude::*;
use mysql::*;
use std::{thread, time};

fn main() {
    // Connection pool
    let pool: Pool =
        Pool::new("mysql://emirde:5AeX3tYs@mysql-ait.stud.idi.ntnu.no/emirde").unwrap();

    // Oppretter to kontoer
    let mut emirs_account = Account::new(pool.get_conn().unwrap(), String::from("Emir"), 200.0);
    let mut kaares_account = Account::new(pool.get_conn().unwrap(), String::from("Kåre"), 100.0);
    emirs_account.update_last_changed(&mut pool.get_conn().unwrap());
    kaares_account.update_last_changed(&mut pool.get_conn().unwrap());
    let mut emir_clone = emirs_account.clone();
    let mut kaare_clone = kaares_account.clone();
    let con1 = pool.get_conn().unwrap();
    let con2 = pool.get_conn().unwrap();
    let con3 = pool.get_conn().unwrap();
    let con4 = pool.get_conn().unwrap();
    let con5 = pool.get_conn().unwrap();
    let thread1 = thread::spawn(move || {
        emir_clone.transfer_funds(&mut kaare_clone, 200.0, con1, con2);
    });
    let thread2 = thread::spawn(move || {
        emirs_account.transfer_funds(&mut kaares_account, 200.0, con3, con4);
        kaares_account.set_owner(con5, String::from("new owner"));
        let mut accounts = Vec::new();
        accounts.push(emirs_account);
        accounts.push(kaares_account);

        for account in accounts_with_balance_over(100.0, &accounts) {
            println!("owner over {} balance: {}", 100, account.get_owner());
        }
    });
    thread1.join();
    thread2.join();
}

fn accounts_with_balance_over(min_balance: f64, accounts: &Vec<Account>) -> Vec<&Account> {
    let mut accounts_over_threashold = Vec::new();
    for account in accounts {
        if account.clone().get_balance() > min_balance {
            accounts_over_threashold.push(account);
        }
    }
    return accounts_over_threashold;
}

#[derive(Clone)]
pub struct Account {
    account_number: u64,
    balance: f64,
    owner: String,
    last_changed: NaiveDateTime,
}

impl Account {
    pub fn new(mut con: PooledConn, owner: String, balance: f64) -> Account {
        con.exec_iter(
            r"INSERT INTO account (balance, owner) VALUES (?, ?)",
            (balance, owner.as_str()),
        );
        let account_number = con.last_insert_id();
        println!("{}", account_number);
        return Account {
            account_number: account_number,
            balance: balance,
            owner: owner,
            last_changed: NaiveDate::from_ymd(2000, 1, 1).and_hms(0, 0, 0),
        };
    }
    pub fn get_balance(&self) -> f64 {
        return self.balance;
    }
    pub fn update_last_changed(&mut self, mut con: &mut PooledConn) -> bool {
        // Get current timestamps
        let mut result = con
            .exec_iter(
                r"SELECT last_changed FROM account WHERE account_number = ?",
                (self.account_number,),
            )
            .unwrap();
        let mut row = result.next().unwrap().unwrap();
        let new_timestamp: NaiveDateTime = row.take("last_changed").unwrap();

        if self.last_changed == new_timestamp {
            return false;
        } else {
            println!(
                "Owner: {}. Last changed: {}, new stamp: {}",
                self.owner, self.last_changed, new_timestamp
            );
            self.last_changed = new_timestamp;
            return true;
        }
    }
    pub fn balance_changed(&mut self, mut con: &mut PooledConn) -> bool {
        let result = con.exec_iter(
            r"SELECT balance FROM account WHERE account_number = ?",
            (self.account_number,),
        );
        let mut row = result.unwrap().next().unwrap().unwrap();
        let new_balance: f64 = row.take("balance").unwrap();
        println!(
            "Balance in DB: {}, previous balance: {}",
            new_balance, self.balance
        );
        // Update timestamp for change
        self.clone().update_last_changed(con);
        if self.balance == new_balance {
            return false;
        }
        self.balance = new_balance;
        return true;
    }
    pub fn get_owner(&self) -> String {
        return self.owner.clone();
    }
    pub fn set_owner(&mut self, mut con: PooledConn, new_owner: String) {
        self.owner = new_owner.clone();
        con.exec_iter(
            r"UPDATE account SET owner = ? WHERE account_number = ?",
            (new_owner.as_str(), self.account_number),
        );
    }
    pub fn get_account_number(&self) -> u64 {
        return self.account_number;
    }
    pub fn subtract_balance(&mut self, amount: f64) {
        self.balance -= amount;
    }
    pub fn add_balance(&mut self, amount: f64) {
        self.balance += amount;
    }
    pub fn transfer_funds(
        &mut self,
        recipiant: &mut Account,
        amount: f64,
        mut con: PooledConn,
        mut con2: PooledConn,
    ) {
        // Optimistic lock
        let mut changed = true;
        while changed {
            changed = false;
            println!("Transfering!");
            // Update values before we start
            self.balance_changed(&mut con);
            recipiant.balance_changed(&mut con);

            // start transaction and update values without commiting
            let mut tx = con.start_transaction(TxOpts::default()).unwrap();
            let new_balance = self.clone().get_balance() - amount;
            let new_recipiant_balance = recipiant.clone().get_balance() + amount;
            tx.exec_drop(
                "update account set balance = ? where account_number = ?;",
                (new_balance, self.clone().get_account_number()),
            );
            tx.exec_drop(
                "update account set balance = ? where account_number = ?;",
                (
                    new_recipiant_balance,
                    recipiant.clone().get_account_number(),
                ),
            );

            let wait = time::Duration::from_millis(1000);
            thread::sleep(wait);

            if self.update_last_changed(&mut con2) || recipiant.update_last_changed(&mut con2) {
                // Someone else has changed something, roll back out changes
                println!("Someone else has changed something, trying again");
                changed = true;
                // Rollback transaction
                tx.rollback();
            } else {
                println!(
                    "Commiting. new balances should be {}: {} and {}: {}",
                    self.clone().owner,
                    new_balance,
                    recipiant.clone().owner,
                    new_recipiant_balance
                );
                // Commit changes, succesfull transaction
                tx.commit();
            }
        }
        // Make the same changes localy
        recipiant.add_balance(amount);
        self.subtract_balance(amount);
    }
}
