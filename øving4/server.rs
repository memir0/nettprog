use std::fs::File;
use std::io::{BufRead, BufReader, Read, Write};
use std::net::{TcpListener, TcpStream};
use std::thread;

fn main() {
    let mut threads = Vec::new();
    let listener = TcpListener::bind("127.0.0.1:5432").unwrap();
    for stream in listener.incoming() {
        let stream = stream.unwrap();
        threads.push(thread::spawn(move || {
            handle_request(stream);
        }))
    }
}

fn handle_request(stream: TcpStream) {
    let mut reader = BufReader::new(stream);

    // Første linje i header
    let mut http_request_definition = String::new();
    let _result = reader.by_ref().read_line(&mut http_request_definition);
    let http_request_definition_split: Vec<&str> = http_request_definition.split_whitespace().collect();
    println!("{}", http_request_definition);

    // Alle headers
    let mut http_request_headers = Vec::new();
    http_request_headers.push(http_request_definition.clone());

    let mut has_body = false;
    if http_request_definition_split[0] == "POST" {
        has_body = true;
    }
    let mut body: Vec<u8>  = vec![];
    for line in reader.by_ref().lines() {
        let line_uw = line.unwrap();
        println!("{}", line_uw);
        if line_uw.len() > 15 {
            if &line_uw[..15] == "Content-Length:"{
                body = vec![0;(&line_uw[16..]).parse().unwrap()]
            }
        }
        if line_uw == "" { 
            if has_body {
                let _result = reader.by_ref().read_exact(&mut body);
            }
            break;
        }
        http_request_headers.push(line_uw);
    }
    if http_request_definition_split[1] == "/favicon.ico" {
        send_file(reader.into_inner(), "rust-logo.png");
    }
    else if http_request_definition_split[1] == "/add" {
        calculator(reader.into_inner(), body, true);
    }
    else if http_request_definition_split[1] == "/sub" {
        calculator(reader.into_inner(), body, false);
    }
    else if http_request_definition_split[1] == "/" {
        send_headers(reader.into_inner(), http_request_headers)
    }
    else if http_request_definition_split[1] == "/kalkulator"{
        send_file(reader.into_inner(), "index.html");
    }
    else {
        send_404(reader.into_inner());
    }
}

fn send_404(mut stream: TcpStream) {
    let response = "HTTP/1.1 404 Not found\n\n";
    stream.write_all(response.as_bytes()).unwrap();
}

fn calculator(mut stream: TcpStream, body: Vec<u8>, add: bool){
    let mut num1_string = String::new(); let mut num2_string = String::new();
    let mut next_num = false;
    for body_char_byte in body {
        let body_char = body_char_byte as char;
        if body_char == ',' {
            next_num = true;
        }
        else if next_num {
            num2_string.push(body_char);
        }
        else {
            num1_string.push(body_char);
        }
    }
    let sum: i64;
    let num1: i64 = num1_string.parse().unwrap();
    let num2: i64 = num2_string.parse().unwrap();
    if add {
        sum = num1 + num2;
    }
    else {
        sum = num1 - num2;
    }
    let mut file_bytes_vec: Vec<u8> = Vec::new();
    // Concat strings
    let response_array = ["HTTP/1.1 200 OK\nContent-Type: application/json; charset=utf-8\nContent-Length: ", &(11 + sum.to_string().len()).to_string()[..], "\n\n{\"answer\":"];
    let response = format!("{}{}{}", response_array[0], response_array[1], response_array[2]);
    for byte in response.as_bytes() {
        file_bytes_vec.push(*byte);
    }
    for sum_string_byte in sum.to_string().as_bytes(){
        file_bytes_vec.push(*sum_string_byte);
    }
    file_bytes_vec.push('}' as u8);
    let file_bytes: &[u8] = &file_bytes_vec;
    let _result = stream.write_all(file_bytes);
}

fn send_headers(mut stream: TcpStream, header: Vec<String>){
    let mut file_bytes_vec: Vec<u8> = Vec::new();
    // Add HTTP response header
    let response = "HTTP/1.1 200 OK\n\n<html><body><h1>Du har koblet deg opp til min enkle web-tjener. Dine headers:</h1><ul>";
    for byte in response.as_bytes() {
        file_bytes_vec.push(*byte);
    }

    // add lines from header
    for line in header{
        let list_wrap_start = "<li>";
        for byte in list_wrap_start.as_bytes(){
            file_bytes_vec.push(*byte);
        }
        for byte in line.as_bytes() {
            file_bytes_vec.push(*byte);
        }
        let list_wrap_end = "</li>";
        for byte in list_wrap_end.as_bytes(){
            file_bytes_vec.push(*byte);
        }
        // Add newline
        file_bytes_vec.push(10);
    }
    let end_line = "</ul><a href='/kalkulator'>Simpel tall kalkulator</a></body></html>";
    for byte in end_line.as_bytes() {
        file_bytes_vec.push(*byte);
    }
    //let temp = &file_bytes_vec; // b: &Vec<u8>
    let file_bytes: &[u8] = &file_bytes_vec;
    stream.write_all(file_bytes).unwrap();
}

fn send_file(mut stream: TcpStream, file_name: &str){
    let mut file_bytes_vec: Vec<u8> = Vec::new();
    let response = "HTTP/1.1 200 OK\n\n";
    for byte in response.as_bytes() {
        file_bytes_vec.push(*byte);
    }

    let mut file = File::open(file_name).unwrap();
    let _result = file.read_to_end(&mut file_bytes_vec);
    //let temp = &file_bytes_vec; // b: &Vec<u8>
    let file_bytes: &[u8] = &file_bytes_vec;
    stream.write_all(file_bytes).unwrap();
}
