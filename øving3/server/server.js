var express = require("express");
var bodyParser = require("body-parser");
const { exec } = require("child_process");
var path = require("path");
const fs = require("fs");
var app = express();
app.use(bodyParser.json());

app.use(express.static("public"));

app.post("/cppEditor", (req, res) => {
  console.log("Fikk hent artikler request fra klient");
  console.log(req.body.code);
  fs.writeFile("docker_gcc/code.cpp", req.body.code, function(err) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
  exec(
    "sudo docker build -t gcc-compiler docker_gcc/.",
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        res.status(400);
        res.json({
          output: stdout
            .split("code.cpp: ")[1]
            .substring(0, stdout.split("code.cpp: ")[1].length - 4)
        });
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        res.status(400);
        res.json({ output: $stderr });
        return;
      }
      console.log(`stdout: ${stdout}`);
      exec("sudo docker run gcc-compiler", (error, stdout, stderr) => {
        if (error) {
          console.log(`error: ${error.message}`);
          res.status(400);
          res.json({ output: error.message });
          return;
        }
        if (stderr) {
          console.log(`stderr: ${stderr}`);
          res.status(400);
          res.json({ output: stderr });
          return;
        }
        console.log(`stdout: ${stdout}`);
        res.status = 200;
        res.json({ output: stdout });
        return;
      });
    }
  );
});

app.post("/rustEditor", (req, res) => {
  console.log("Fikk hent artikler request fra klient");
  console.log(req.body.code);
  fs.writeFile("docker_rust/code.rs", req.body.code, function(err) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
  exec(
    "sudo docker build -t rust_compiler docker_rust/.",
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        res.status(400);
        res.json({
          output: stdout
            .split("Step 4/5 : RUN rustc -o myapp code.rs")[1]
            .substring(
              35,
              stdout.split("Step 4/5 : RUN rustc -o myapp code.rs")[1].length -
                54
            )
        });
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        res.status(400);
        res.json({ output: $stderr });
        return;
      }
      console.log(`stdout: ${stdout}`);
      exec("sudo docker run rust_compiler", (error, stdout, stderr) => {
        if (error) {
          console.log(`error: ${error.message}`);
          res.status(400);
          res.json({ output: error.message });
          return;
        }
        if (stderr) {
          console.log(`stderr: ${stderr}`);
          res.status(400);
          res.json({ output: stderr });
          return;
        }
        console.log(`stdout: ${stdout}`);
        res.status = 200;
        res.json({ output: stdout });
        return;
      });
    }
  );
});

app.post("/javaEditor", (req, res) => {
  console.log("Fikk hent artikler request fra klient");
  console.log(req.body.code);
  fs.writeFile("docker_java/code.java", req.body.code, function(err) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
  exec(
    "sudo docker build -t java_compiler docker_java/.",
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        res.status(400);
        res.json({
          output: stdout
            .split("Step 4/5 : RUN javac code.java")[1]
            .substring(
              49,
              stdout.split("Step 4/5 : RUN javac code.java")[1].length - 21
            )
            .replace("[0m[91m", "")
        });
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        res.status(400);
        res.json({ output: $stderr });
        return;
      }
      console.log(`stdout: ${stdout}`);
      exec("sudo docker run java_compiler", (error, stdout, stderr) => {
        if (error) {
          console.log(`error: ${error.message}`);
          res.status(400);
          res.json({ output: error.message });
          return;
        }
        if (stderr) {
          console.log(`stderr: ${stderr}`);
          res.status(400);
          res.json({ output: stderr });
          return;
        }
        console.log(`stdout: ${stdout}`);
        res.status = 200;
        res.json({ output: stdout });
        return;
      });
    }
  );
});

app.post("/haskellEditor", (req, res) => {
  console.log("Fikk hent artikler request fra klient");
  console.log(req.body.code);
  fs.writeFile("docker_haskell/code.hs", req.body.code, function(err) {
    if (err) {
      return console.log(err);
    }
    console.log("The file was saved!");
  });
  exec(
    "sudo docker build -t haskell_compiler docker_haskell/.",
    (error, stdout, stderr) => {
      if (error) {
        console.log(`error: ${error.message}`);
        res.status(400);
        res.json({
          output: stdout
            .split("Step 4/5 : RUN ghc -o code code.hs")[1]
            .substring(
              40,
              stdout.split("Step 4/5 : RUN ghc -o code code.hs")[1].length - 5
            )
            .replace("[0m[91m2", "")
            .replace("[0m[91m", "")
            .replace("[91m", "")
        });
        return;
      }
      if (stderr) {
        console.log(`stderr: ${stderr}`);
        res.status(400);
        res.json({ output: $stderr });
        return;
      }
      console.log(`stdout: ${stdout}`);
      exec("sudo docker run haskell_compiler", (error, stdout, stderr) => {
        if (error) {
          console.log(`error: ${error.message}`);
          res.status(400);
          res.json({ output: error.message });
          return;
        }
        if (stderr) {
          console.log(`stderr: ${stderr}`);
          res.status(400);
          res.json({ output: stderr });
          return;
        }
        console.log(`stdout: ${stdout}`);
        res.status = 200;
        res.json({ output: stdout });
        return;
      });
    }
  );
});

let port = 8080;
console.log("listening on port: " + port);
var server = app.listen(port);
