extern crate base64;
extern crate sha1;
use sha1::{Digest, Sha1};
use std::fs::File;
use std::io::{BufRead, BufReader, Read, Write};
use std::net::{TcpListener, TcpStream};
use std::sync::{Arc, Condvar, Mutex};
use std::thread;

fn main() {
    let http_server = thread::spawn(move || {
        let mut threads = Vec::new();
        let listener = TcpListener::bind("127.0.0.1:3000").unwrap();
        for stream in listener.incoming() {
            let stream = stream.unwrap();
            threads.push(thread::spawn(move || {
                handle_request(stream);
            }))
        }
    });
    let websocket_server =
        thread::spawn(move || {
            let brodcast_message = Arc::new((Mutex::new(String::new()), Condvar::new()));
            let mut threads = Vec::new();
            let listener = TcpListener::bind("127.0.0.1:3001").unwrap();
            for stream in listener.incoming() {
                let stream = stream.unwrap();
                let brodcast_message_clone = brodcast_message.clone();
                threads.push(thread::Builder::new().name("websocket".to_string()).spawn(
                    move || {
                        handle_websocket(stream, brodcast_message_clone);
                    },
                ))
            }
        });

    // spoiler: they never finish :O
    http_server.join();
    websocket_server.join();
}

fn handle_websocket(mut stream: TcpStream, brodcast_message: Arc<(Mutex<String>, Condvar)>) {
    let mut reader = BufReader::new(&stream);

    // Første linje i header
    let mut http_request_definition = String::new();
    let mut _result = reader.by_ref().read_line(&mut http_request_definition);
    let http_request_definition_split: Vec<&str> =
        http_request_definition.split_whitespace().collect();
    println!("{}", http_request_definition);

    // Alle headers
    let mut http_request_headers = Vec::new();
    http_request_headers.push(http_request_definition.clone());

    let mut key: String = String::new();
    for line in reader.by_ref().lines() {
        let line_uw = line.unwrap();
        // println!("{}", line_uw);
        if line_uw.len() > 18 {
            if &line_uw[..18] == "Sec-WebSocket-Key:" {
                key = String::from(&line_uw[19..]);
            }
        }
        if line_uw == "" {
            break;
        }
        http_request_headers.push(line_uw);
    }
    send_handshake(&stream, key);

    // To send out a brodcast message to all clients, they must all listen, thats why there is a thread here listening
    let brodcast_message_clone = brodcast_message.clone();

    // We make a copy of the tcp stream for writing back
    let mut wstream = stream.try_clone().unwrap();

    // This lock tells the listener to stop
    let connection_closed = Arc::new(Mutex::new(false));
    let connection_closed_clone = connection_closed.clone();

    // This thread listens for a new message from an external thread to brodcast
    let broadcast_message_listner = thread::Builder::new()
        .name("broadcaster".to_string())
        .spawn(move || loop {
            let (message, cvar) = &*brodcast_message_clone;
            let mut message = message.lock().unwrap();
            message = cvar.wait(message).unwrap();
            let connection_closed = connection_closed_clone.lock().unwrap();
            if *connection_closed {
                println!("Connection closed -broadcast_message_listner");
                break;
            }
            println!("Broadcast message: {}", message);
            let mut formated_message: Vec<u8> = Vec::new();
            // basically means the message is good and its the entire message
            formated_message.push(129u8);
            /*
            |    Payload length, also denotes that payload is unmasked.
            |    This is caped at 126 payload length, for longer messages
            V    a better implementation is required.
            */
            formated_message.push(message.len() as u8);

            for byte in message.as_bytes() {
                formated_message.push(*byte);
            }
            wstream.write_all(&formated_message);
        });

    // Handle communications
    loop {
        let mut client_message = vec![0u8; 2];
        let result = reader.by_ref().read(&mut client_message);
        // If TCP stream abruptly closes, the loop ends
        if (result.unwrap() == 0) {
            println!("Connection lost.");
            break;
        }

        let op_code = client_message[0] & 15;
        let mut payload_length: u32 = (client_message[1] as u32) & 127; // Ignore first bit
        println!("payload length: {}", payload_length);
        println!("Op code: {}", op_code);

        // Client cleanly closes connection considering coresponding causality cases
        if (op_code == 8) {
            println!("Connection closed.");
            break;
        }

        // Get masking key
        let mut masking_key = vec![0u8; 4];
        _result = reader.by_ref().read(&mut masking_key);

        // Get payload
        let mut client_payload = vec![0u8; payload_length as usize];
        _result = reader.by_ref().read(&mut client_payload);

        // Decoding payload with masking key
        let mut decoded_message = String::new();
        let mut i = 0;
        for byte in client_payload {
            decoded_message.push((byte ^ masking_key[i % 4]) as char);
            i += 1;
        }
        println!("Payload: {}", decoded_message);

        // Send payload to all clients
        let (message, cvar) = &*brodcast_message;
        let mut message = message.lock().unwrap();
        *message = decoded_message;
        cvar.notify_all();
    }
    {
        let mut connection_closed = connection_closed.lock().unwrap();
        *connection_closed = true;
    }
    broadcast_message_listner.unwrap().join();
}

fn send_handshake(mut stream: &TcpStream, key: String) {
    // intial response setup
    let response = "HTTP/1.1 101 Switching Protocols\nUpgrade: websocket\nConnection: Upgrade\nSec-WebSocket-Accept: ";
    let mut response_bytes: Vec<u8> = Vec::new();
    for byte in response.bytes() {
        response_bytes.push(byte);
    }

    // Handshake calculation
    // concating the client string with the magic string
    let magic_string = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    let mut concat = String::from(key);
    concat.push_str(magic_string);

    // Sha1 hashing the string
    let mut hasher = Sha1::new();
    hasher.input(concat);
    let hash = &hasher.result()[..];

    // Encoding the result as base64 and appending it to the headers. [u8] -> String
    let hash_string = base64::encode(&hash);
    for byte in hash_string.as_bytes() {
        response_bytes.push(*byte);
    }

    // Header end
    for byte in "\n\n".as_bytes() {
        response_bytes.push(*byte);
    }

    stream.write_all(&response_bytes).unwrap();
}

fn handle_request(stream: TcpStream) {
    let mut reader = BufReader::new(stream);

    // Første linje i header
    let mut http_request_definition = String::new();
    let _result = reader.by_ref().read_line(&mut http_request_definition);
    let http_request_definition_split: Vec<&str> =
        http_request_definition.split_whitespace().collect();
    println!("{}", http_request_definition);

    // Alle headers
    let mut http_request_headers = Vec::new();
    http_request_headers.push(http_request_definition.clone());

    let mut has_body = false;
    if http_request_definition_split[0] == "POST" {
        has_body = true;
    }
    let mut body: Vec<u8> = vec![];
    for line in reader.by_ref().lines() {
        let line_uw = line.unwrap();
        // println!("{}", line_uw);
        if line_uw.len() > 15 {
            if &line_uw[..15] == "Content-Length:" {
                body = vec![0; (&line_uw[16..]).parse().unwrap()]
            }
        }
        if line_uw == "" {
            if has_body {
                let _result = reader.by_ref().read_exact(&mut body);
            }
            break;
        }
        http_request_headers.push(line_uw);
    }
    if http_request_definition_split[1] == "/" {
        send_file(reader.into_inner(), "index.html")
    } else if http_request_definition_split[1] == "/favicon.ico" {
        send_file(reader.into_inner(), "rust-logo.png");
    } else {
        send_404(reader.into_inner());
    }
}

fn send_404(mut stream: TcpStream) {
    let response = "HTTP/1.1 404 Not found\n\n";
    stream.write_all(response.as_bytes()).unwrap();
}

fn send_file(mut stream: TcpStream, file_name: &str) {
    let mut file_bytes_vec: Vec<u8> = Vec::new();
    let response = "HTTP/1.1 200 OK\n\n";
    for byte in response.as_bytes() {
        file_bytes_vec.push(*byte);
    }

    let mut file = File::open(file_name).unwrap();
    let _result = file.read_to_end(&mut file_bytes_vec);
    //let temp = &file_bytes_vec; // b: &Vec<u8>
    let file_bytes: &[u8] = &file_bytes_vec;
    stream.write_all(file_bytes).unwrap();
}
