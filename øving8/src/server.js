const express = require('express')
const path = require('path')
const app = express()
const port = 3000

app.use(express.static(path.join(__dirname, '../public')));

app.get('/secret_room', function (req, res) {
  res.send('<a href="https://www.youtube.com/watch?v=k85mRPqvMbE">click me<\\a>')
})

app.get('/super_secret_room', function (req, res) {
  res.send('<img src="https://i.imgur.com/905TeUk.gif" alt="smiley gif">')
})

// The listen promise can be used to wait for the web server to start (for instance in your tests)
exports.appListen = new Promise((resolve, reject) => {
  app.listen(port, (error) => {
    if (error) return reject(error.message);

    console.log('Express server started');
    resolve();
  });
});
