use std::net::UdpSocket;

fn main() {
    let socket = UdpSocket::bind("127.0.0.1:5432").expect("couldn't bind to address");
    let mut num1_bytes = [0; 4];
    let mut num2_bytes = [0; 4];
    let (mut _number_of_bytes, src) = socket.recv_from(&mut num1_bytes).expect("Didn't receive data");
    _number_of_bytes = socket.recv(&mut num2_bytes).expect("Didn't receive data");

    let mut num1 = 0;
    let mut num2 = 0;

    let mut num1_is_negative = false;
    let mut num2_is_negative = false;

    if num1_bytes[0] >= 128{
        num1_bytes[0] -= 128;
        num1 = std::i32::MAX;
        num1_is_negative = true;
    }

    if num2_bytes[0] >= 128{
        num2_bytes[0] -= 128;
        num2 = std::i32::MAX;
        num2_is_negative = true;
    }

    if num1_is_negative {
        for i in 0..4 {
            num1 -= 256_i32.pow(3-i)*(num1_bytes[i as usize] as i32);
        }
        num1 = -(num1+1);
    } else {
        for i in 0..4 {
            num1 += 256_i32.pow(3-i)*(num1_bytes[i as usize] as i32);
        }
    }

    if num2_is_negative {
        for i in 0..4 {
            num2 -= 256_i32.pow(3-i)*(num2_bytes[i as usize] as i32);
        }
        num2 = -(num2+1);
    } else {
        for i in 0..4 {
            num2 += 256_i32.pow(3-i)*(num2_bytes[i as usize] as i32);
        }
    }

    println!("num1: {}, num2: {}", num1, num2);

    let mut method_bytes = [0; 3];
    let _result = socket.recv(&mut method_bytes);

    let mut method = String::new();
    for byte in method_bytes.iter() {
        method.push(*byte as char);
    }

    println!("Method: {}", method);

    if method == "add" {
        let result = num1 + num2;
        println!("Result: {}", result);
        socket.send_to(&result.to_be_bytes(), &src);
    } else {
        let result = num1 - num2;
        println!("Result: {}", result);
        socket.send_to(&result.to_be_bytes(), &src);
    }
}