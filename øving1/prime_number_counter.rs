use std::io;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Instant;

fn main() {
	// Arc: thread-safe reference counted object.
	// Mutex: data and mutex combined, where the data cannot be access without locking the mutex
	let prime_numbers_arc = Arc::new(Mutex::new(vec![0; 0]));
	let sum_mutex_arc = Arc::new(Mutex::new(0));
	let mut start = read_input("start");
	// Input validation
	if start < 2 {
		start = 2;
	}
	let mut start_sum = sum_mutex_arc.lock().unwrap();
	*start_sum = start;
	std::mem::drop(start_sum);
	let mut end = read_input("end");
	// Input validation
	if end < 2 {
		end = 2;
	}

	let amount_of_threads = read_input("amount of threads");
	let mut threads = Vec::new();
	// Timer
	let now = Instant::now();
	for _ in 0..amount_of_threads {
		let sum_mutex_arc_copy = sum_mutex_arc.clone();
		let prime_numbers_arc_copy = prime_numbers_arc.clone();
		threads.push(thread::spawn(move || {
			// Access the data by locking the Mutex object
			let mut sum_locked = sum_mutex_arc_copy.lock().unwrap();
			*sum_locked += 1;
			let mut sum = *sum_locked;
			std::mem::drop(sum_locked);
			while sum <= end {
				// Find out if number is prime
				if sum == 2 {
					let mut prime_numbers = prime_numbers_arc_copy.lock().unwrap();
					prime_numbers.push(2);
				} else if sum == 3 {
					let mut prime_numbers = prime_numbers_arc_copy.lock().unwrap();
					prime_numbers.push(3);
				} else if sum % 2 == 0 || sum % 3 == 0 { //Do nothing
				} else {
					let mut index = 5;
					let mut is_prime = true;
					while index * index <= sum {
						if sum % index == 0 || sum % (index + 2) == 0 {
							is_prime = false;
							break;
						}
						index += 6;
					}
					if is_prime {
						let mut prime_numbers = prime_numbers_arc_copy.lock().unwrap();
						prime_numbers.push(sum);
					}
				}
				// Get new number from lock
				sum_locked = sum_mutex_arc_copy.lock().unwrap();
				*sum_locked += 1;
				sum = *sum_locked;
				std::mem::drop(sum_locked);
			}
		}))
	}
	//println!("Threads length: {}", type_of(threads));
	for thread in threads {
		let _ = thread.join();
	}
	//println!("Threads length: {}", threads.len());
	for prime in prime_numbers_arc.lock().unwrap().iter() {
		println!("{}", prime);
	}
	println!("Time elapsed: {}", now.elapsed().as_secs());
}

use std::any::type_name;

fn type_of<T>(_: T) -> &'static str {
	type_name::<T>()
}

fn read_input(input_type: &str) -> u32 {
	println!("Please input {}", input_type);
	let mut input_text = String::new();
	io::stdin()
		.read_line(&mut input_text)
		.expect("failed to read from stdin");

	let trimmed = input_text.trim();
	match trimmed.parse::<u32>() {
		Ok(i) => return i,
		Err(..) => {
			println!("this was not an integer: {}", trimmed);
			return read_input(input_type);
		}
	};
}
